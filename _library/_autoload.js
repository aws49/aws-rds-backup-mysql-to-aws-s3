/**
 * Local Library Module.
 * Exports ALL modules.
 */
module.exports = {
    // AWS SDK:
    aws_requests: require('./aws_requests'),
    aws_s3: require('./aws_s3'),
    aws_rds: require('./aws_rds'),
    // Classes:
    MySQL: require('./MySQL'),
    // Other:
    user_requests: require('./user_requests'),
    date_time: require('./date_time'),
    process: require('./process'),
    promise_queue: require('./promise_queue'),
};
