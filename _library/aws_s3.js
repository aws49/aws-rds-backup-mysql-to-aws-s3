/**
 * Custom AWS S3 Module.
 *
 * Consists useful functions and methods for
 * work with AWS S3 via AWS SDK.
 */

/**
 * NPM Modules
 *
 * Documentation:
 * @module aws_requests - ./aws_requests.js, module with request
 *                         methods to AWS Services.
 */
const aws_requests = require('./aws_requests');
const stream = require('stream');

/**
 * Exports Module Functions.
 */
module.exports = {

    /**
     * Check Bucket Exists.
     * Searches bucket in list of buckets.
     *
     * @param {string} bucket
     *      A bucket name, which we search.
     *
     * @return {Promise<boolean>}
     *      true - is exists, false - is NOT exist.
     */
    check_bucket_exist: (bucket='') => aws_requests.s3.list_buckets().then(data =>
        data.Buckets.some(elem => elem.Name === bucket))
    ,

    /**
     * Get List of Buckets.
     * Requests list of buckets to AWS S3 via SDK.
     *
     * @return {Promise<string[]>}
     *      A list of buckets names.
     */
    get_list_buckets: () => aws_requests.s3.list_buckets().then(data =>
        data.Buckets.map(elem => elem.Name))
    ,

    /**
     * Get List of Objects.
     *  Requests list of buckets to AWS S3 via SDK.
     *
     * @return {Promise<string[]>}
     *      A list of objects keys.
     */
    get_list_objects: (params={}) => aws_requests.s3.list_objects(params).then(data =>
        data.Contents.map(elem => elem.Key))
    ,

    /**
     * Delete Folder of S3 bucket.
     * Deletes full folder in the bucket.
     *
     * @param {Object} params
     *      A parameters for listObject() AWS RDS Request.
     *      Params:
     *      {
     *          Bucket: '',
     *          Prefix: ''
     *      }
     *
     * @return {Promise<Response>}
     *      A response of objects deleting.
     */
    delete_folder: function (params={Bucket: '', Prefix: ''}) {

        const delete_params = {
            Bucket: params.Bucket,
            Delete: {
                Objects: []
            }
        };

        return new Promise(resolve => {
            this.get_list_objects(params).then(objects => {
                if (objects.length === 0)
                    resolve(objects);

                objects.forEach(item => delete_params.Delete.Objects.push({ Key: item }));

                aws_requests.s3.delete_objects(delete_params).then(response => {
                    if(objects.length === 1000)
                        resolve(this.delete_folder(params));

                    resolve(response);
                });
            });
        });
    },

    /**
     * Upload Object From Stream.
     * Uploads object to AWS S3 bucket via Stream.
     *
     * @param {string} key
     *      A key of object.
     * @param {string} bucket
     *      A bucket name.
     * @param callback
     *      A callback with a response of uploadObject() Request
     *      in arguments.
     *
     * @return {Object<PassThrough>}
     */
    upload_from_stream: (key='', bucket='', callback) => {

        const pass = new stream.PassThrough();

        console.log(
            'Uploading asynchronously the object to AWS S3 bucket...\n',
            `Object key: ${key}\n`,
            `S3 Bucket: ${bucket}`
        );

        App.lib.aws_requests.s3.upload_object({
            Bucket: bucket,
            Key: key,
            Body: pass
        })
        .then((response) => {
            console.log('<<< [!] Object is uploaded. >>>');

            callback(response);
        });

        return pass;
    },

};
