/**
 * MySQL Module Class.
 *
 * Consists of useful MySQL class for
 * working with MySQL.
 */

/**
 * NPM Modules.
 *
 * Documentation:
 * @module mysql - https://www.npmjs.com/package/mysql
 */
const _mysql = require('mysql');

/**
 * MySQL Class.
 *
 * Constructor params:
 * mysql_connect_params = {
 *           host: '',
 *           user: '',
 *           password: '',
 *           database: ''
 *      }
 */
module.exports = class MySQL {

    // Variables:
    #con;

    constructor(mysql_connect_params={}) {
        this.#con = _mysql.createConnection(mysql_connect_params);
    }

    // [Private] Creates a connection to MySQL
    #connect = () => new Promise(resolve => {
        if (this.#con._connectCalled)
        {
            console.log('[!] The connection to MySQL ALREADY exists!');

            resolve();
        }
        else
            this.#con.connect(err => {
                if (err)
                    throw new Error(err);

                console.log('[!] Connected to MySQL!');

                resolve();
            });
    });

    // Disconnects from MySQL
    disconnect = () => new Promise(resolve => this.#con.end(err => {
            if (err)
                throw new Error(err);

            console.log('[!] Disconnected from MySQL!');

            resolve();
        }));

    /**
     * Make a query to MySQL.
     *
     * @param {string} query
     *      A SQL command.
     *
     * @return {Promise<any>}
     *      A result of the query.
     */
    makeQuery = (query='') => new Promise(resolve => this.#connect().then(() =>
        this.#con.query(query, (err, result) => {
            if (err)
                throw new Error(err);

            resolve(result);
        })));

};
