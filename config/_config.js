
let app_name = 'aws-scripts';

// Config must be available for the whole application
module.exports = {
    app_name,

    default_timezone: 'Australia/Sydney',
    aws_credentials_path: __dirname + '/aws-credentials.json'
};
